package com.founder.core.base;

import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class BaseDo<T> implements Serializable{

    private Class<T> entity;

    //表名称
    private String tableName;
    //字段信息
    private List<BaseFieldInfo> fields;
    //更新或者删除时的条件，保留使用
    private List<String> whereFields;
    //数据主键
    private List<String> primaryKeyList;
    //查询出来的原始记录
    private ArrayList<T> selectList = new ArrayList<>();
    //新增记录集
    private ArrayList<T> insertList = new ArrayList<>();
    //更新记录集
    private ArrayList<T> updateList = new ArrayList<>();
    //删除记录集
    private ArrayList<T> deleteList = new ArrayList<>();

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<BaseFieldInfo> getFields() {
        return fields;
    }


    public List<String> getWhereFields() {
        return whereFields;
    }


    public List<String> getprimaryKeyList() {
        return primaryKeyList;
    }

    public void setprimaryKeyList(List<String> primaryKeyList) {
        this.primaryKeyList = primaryKeyList;
    }

    public ArrayList<T> getSelectList() {
        return selectList;
    }


    public ArrayList<T> getInsertList() {
        return insertList;
    }


    public ArrayList<T> getUpdateList() {
        return updateList;
    }


    public ArrayList<T> getDeleteList() {
        return deleteList;
    }

    public void setDeleteList(ArrayList<T> deleteList) {
        this.deleteList = deleteList;
    }


    public BaseDo() {
        entity = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
//      FieldMeta filed = entity.getAnnotation(FieldMeta.class);

        if (this.entity != null) {

            /**返回类中所有字段，包括公共、保护、默认（包）访问和私有字段，但不包括继承的字段
             * entity.getFields();只返回对象所表示的类或接口的所有可访问公共字段
             * 在class中getDeclared**()方法返回的都是所有访问权限的字段、方法等；
             * 可看API
             * */
            Table table = entity.getAnnotation(Table.class);
            if (table != null) {
                setTableName(table.name());
            }
            Field[] declaredFields = entity.getDeclaredFields();
            primaryKeyList = Arrays.stream(declaredFields).filter(x->{
                Id id =x.getAnnotation(Id.class);
                return id !=null;
            }).map(x->x.getName()).collect(toList());
//
            fields = Arrays.stream(declaredFields).map(f -> {
                BaseFieldInfo fieldInfo = new BaseFieldInfo();
                fieldInfo.setFieldName(f.getName());
                fieldInfo.setFieldKind(f.getType().getSimpleName().toString());
                return fieldInfo;
            }).collect(toList());
            Arrays.stream(declaredFields).filter(

                f-> f.getAnnotation(Table.class) != null

            ).map(f->f.getName()).collect(toList());


        }


    }
}
